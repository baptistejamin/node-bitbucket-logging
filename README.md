Node BitBucket Logging
===================

Node BitBucket Logging is a custom log issue handler that has been written with the purpose of auto-opening (and assigning) issues on BitBucket everytime something goes south with NodeJS code.

Useful for production deployments, where you want to track the issues directly from BitBucket, not from a dark server log file.

Node BitBucket Logging is smart enough to recognize similar errors, thus not opening blindly a new issue everytime.


## Setup

* Add `bitbucket-logging` to your package.json dependencies.

* Then, require and configure the module using the code below:

```javascript
bitbucket_logging = require('bitbucket-logging');

/* GitLab options */
bitbucket_logging.configure({
    //Required
    slug:"PROJECT_NAME",
    owner:"OWNER",
    username:"USERNAME",
    password:"PASSWORD",

    // Optional
    environment: 'production'           // The NodeJS environment in use, useful when you pre-process the NODE_ENV value
});
```

* Then, when you need to handle an error, just call:

```javascript
// Boom, the error variable value will be redirected to your Bitbucket issues tracker!
bitbucket_logging.handle(error, callback);
```

* If you need to catch all unhandled errors (which is recommended!), use the following code:

```javascript
process.on('uncaughtException', function(error) {
    var error_message = error.stack || error;

    // Log to console
    console.error('uncaughtException', error_message);

    // Pipe error to Bitbucket
    bitbucket_logging.handle(error_message, function{
        // Recommended: kill the NodeJS process (restart a clean one via forever)
        process.exit(1);
    });
});
```

* Enjoy!

## Notes

* This module is based on https://github.com/valeriansaliou/node-gitlab-logging , Valerian Saliou.
* This module is in production in the Yool Core [Yool](http://getyool.co/)
