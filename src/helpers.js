/*
* Node BitBucket Logging
*
* Copyright 2014, Valerian Saliou, Baptiste Jamin
* Author: Valerian Saliou <valerian@valeriansaliou.name>
*/


const NS = 'bitbucket-logging/helpers';


// Import libs
const log = require('loglevel');
const crypto = require('crypto');


// Process issue checksum
function __checksum(error) {
    const FN = '[' + NS + '.__checksum' + ']';

    return crypto.createHash('md5').update(error).digest('hex');
}


// Process issue data
function __data(error, options, checksum) {
    const FN = '[' + NS + '.__data' + ']';

    var description = {
        head: '#### :zap: Note: this issue has been automatically opened.',
        trace: '```javascript\n' + error + '\n```'
    };

    var data = {};

    data.title = ('[ERROR@' + options.environment + '] Events Server Exception (' + checksum + ')');
    data.description = description.head + '\n\n---\n\n' + description.trace;

    return data;
}


// Handles project list from BitBucket
function __handle_list(repository, options, error, issues, issue_data, callback) {
    const FN = '[' + NS + '.__handle_list' + ']';

    try {
        if(error !== null) {
            log.error(FN, 'Could not list issues from BitBucket');
            if(typeof callback == 'function') {
                callback();
            }
        } else {
            var existing_issue_id = null;
            var existing_issue_state = null;

            for(var i in issues) {
                if(issues[i].title == issue_data.title) {
                    existing_issue_id = issues[i].local_id;
                    existing_issue_state = issues[i].status;

                    break;
                }
            }

            if(existing_issue_id !== null) {
                if(existing_issue_state !== 'open') {
                    __reopen(repository, options, existing_issue_id, callback);
                } else {
                    if(typeof callback == 'function') {
                        callback();
                    }
                    log.info(FN, 'Issue exists and is already opened, not re-opening');
                }
            } else {
                __create(repository, options, issue_data, callback);
            }
        }
    } catch(_e) {
        log.error(FN, _e);
        if(typeof callback == 'function') {
            callback();
        }
    }
}


// Reopens a closed issue
function __reopen(repository, options, existing_issue_id, callback) {
    const FN = '[' + NS + '.__reopen' + ']';

    repository.issues().update(existing_issue_id, {
        status: 'open',
        priority: 'major'
    }, function(error, row) {
        if(typeof callback == 'function') {
            callback();
        }
        __handle_reopen(error, row);
    });
}

// Handles the reopening response
function __handle_reopen(error, row) {
    const FN = '[' + NS + '.__handle_reopen' + ']';

    try {
        if(error !== null || row.status !== 'open') {
            log.error(FN, 'Could not re-open existing issue on BitBucket');
        } else {
            log.info(FN, 'Re-opened existing issue on BitBucket');
        }
    } catch(_e) {
        log.error(FN, _e);
    }
}


// Creates a new issue
function __create(repository, options, issue_data, callback) {
    const FN = '[' + NS + '.__create' + ']';

    repository.issues().create({
        title: issue_data.title,
        content: issue_data.description,
        labels: 'node, error, bug',
        status: "open"
    }, function(error, row) {
        if(typeof callback == 'function') {
            callback();
        }
        __handle_create(error, row);
    });
}


// Handles the creation response
function __handle_create(error, row) {
    const FN = '[' + NS + '.__handle_create' + ']';

    try {
        if(error !== null) {
            console.log(error);
            log.error(FN, 'Could not open issue on BitBucket');
        } else {
            log.info(FN, 'Opened issue on BitBucket');
        }
    } catch(_e) {
        log.error(FN, _e);
    }
}


// Engages the issue opening process
exports.__engage = function(repository, error, options, callback) {
    const FN = '[' + NS + '.__engage' + ']';

    try {
        log.info(FN, 'Engaging BitBucket issue opening process...');

        // Process issue SHA-1 checksum
        var checksum = __checksum(error);

        // Process issue data
        var issue_data = __data(error, options, checksum);

        // Check if issue already exists
        repository.issues().get({}, function(error, data) {
            __handle_list(repository, options, error, data.issues, issue_data, callback);
        });
    } catch(_e) {
        log.error(FN, _e);
        if(typeof callback == 'function') {
            callback();
        }
    }
};
