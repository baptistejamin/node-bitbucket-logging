/*
 * Node GitLab Logging
 *
 * Copyright 2014, Valerian Saliou
 * Authors: Valerian Saliou <valerian@valeriansaliou.name>
 */


const NS = 'bitbucket-logging';


// Import libs
const log = require('loglevel');
const url = require('url');
const bitbucket_api = require('bitbucket-api');


// Import modules
const helpers = require('./src/helpers');


// Globals
var OPTIONS = null;


// Configures options
exports.configure = function(options) {
    if(options.username === undefined || options.password === undefined ||
       options.slug === undefined || options.owner === undefined) {
        log.error(FN, 'A required argument is missing, not saving configuration');
        return false;
    }

    if(options.environment === null) {
        options.environment = process.env.NODE_ENV || 'development';
    }

    OPTIONS = options;

    return true;
};


// Sets log level
exports.set_loglevel = function(level) {
    log.setLevel(level);
};


// Handles an incoming error stacktrace
exports.handle = function(error, callback) {
    const FN = '[' + NS + '.handle' + ']';

    if(OPTIONS === null) {
        log.error(FN, 'Please configure the module before using it! Usage: configure(options)');
        return;
    }

    const bitbucket_client = bitbucket_api.createClient({username: OPTIONS.username, password: OPTIONS.password});

    const repository = bitbucket_client.getRepository({slug: OPTIONS.slug, owner: OPTIONS.owner}, function (err, repo) {
      if(repo !== null && !err) {
        helpers.__engage(repo, error, OPTIONS, callback);
      }
      else{
        log.error(FN, 'BitBucket API not reached, please check your configuration')
        callback();
      }
    });

};
